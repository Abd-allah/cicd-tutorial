# Use the official Node.js 16 image as the base
FROM node:18

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application files to the container
COPY . .

# Build the application
RUN npm run build

# Expose the port that the application listens on
EXPOSE 3000

# Start the application
CMD ["npm", "run", "start"]